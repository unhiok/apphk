﻿/// <summary>
/// 取得點數兌換選單
/// </summary>

namespace GetRedeemOptions
{
    public class OptionsInput
    {
        public string CustNo { set; get; } = "";
    }

    public class OptionsItem
    {
        public string Code { set; get; } = string.Empty;
        public string Name { set; get; } = string.Empty;
    }

    public class OptionsOutput
    {
        public string RC { set; get; } = "";
        public string RM { set; get; } = "";
        //排序選單
        public List<OptionsItem> RedeemSortMenu { set; get; } = new List<OptionsItem>();
        //兌換類別選單         
        public List<OptionsItem> RedeemClassMenu { set; get; } = new List<OptionsItem>();

        //點數區間選單 
        public List<OptionsItem> RedeemPointMenu { set; get; } = new List<OptionsItem>();
        //門市選單 
        public List<OptionsItem> RedeemStoreMenu { set; get; } = new List<OptionsItem>();
        //狀態選單 
        public List<OptionsItem> RedeemStatusMenu { set; get; } = new List<OptionsItem>();
        //兌換方式選單 
        public List<OptionsItem> RedeemWaysMenu { set; get; } = new List<OptionsItem>();


    }
}

/// <summary>
/// 進行兌換檢查
/// </summary>
/// 

namespace CheckRedeem
{
    public class CheckInput
    {
        public string CustNo { set; get; } = string.Empty;
        public string PointRedeemNo { set; get; } = string.Empty;
        public int Point { set; get; } = 0;
        public int Plus { set; get; } = 0;
        public List<CheckOrderList> OrderList { set; get; } = new List<CheckOrderList>();
    }

    public class CheckOrderList
    {
        public string RedeemProdNo { set; get; } = string.Empty;
        public int QTY { set; get; } = 0;
    }

    public class CheckOutput
    {
        public string RC { set; get; } = string.Empty;
        public string RM { set; get; } = string.Empty;
    }

}


/// <summary>
/// 進行點數兌換
/// </summary>
namespace OrderRedeem
{
    public class BaseModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string RC { get; set; } = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public string RM { get; set; } = string.Empty;
    }


    public class OrderInput
    {

        /// <summary>
        /// 區域(TW)
        /// </summary>
        public string Area { set; get; } = string.Empty;
        /// <summary>
        /// 語系(b)
        /// </summary>
        public string Lang { set; get; } = string.Empty;
        /// <summary>
        /// 兌換品編號
        /// </summary>
        public string PointRedeemNo { set; get; } = string.Empty;
        /// <summary>
        /// 會員編號
        /// </summary>
        public string CustNo { set; get; } = string.Empty;
        /// <summary>
        /// 領取方式
        /// </summary>
        public string ReceiveWay { set; get; } = string.Empty;
        /// <summary>
        /// 收件人
        /// </summary>
        public string Recipient { set; get; } = string.Empty;
        /// <summary>
        /// 領取地址
        /// </summary>
        public string Address { set; get; } = string.Empty;
        /// <summary>
        /// 連絡電話
        /// </summary>
        public string ReceivePhone { set; get; } = string.Empty;
        /// <summary>
        /// 領取門市代碼
        /// </summary>
        public string ReceiveStore { set; get; } = string.Empty;
        /// <summary>
        /// 資料來源
        /// </summary>
        public string DataType { set; get; } = string.Empty;
        /// <summary>
        /// 兌換品項
        /// </summary>
        public List<OrderList> OrderList { set; get; } = new List<OrderList>();
    }

    public class OrderList
    {
        /// <summary>
        /// 品項編號
        /// </summary>

        public string RedeemProdNo { set; get; } = string.Empty;

        /// <summary>
        /// 兌換數量
        /// </summary>
        public int QTY { set; get; } = 0;
    }

    public class OrderOutput
    {
        /// <summary>
        /// 回傳代碼
        /// </summary>
        /// 

        public string RC { set; get; } = string.Empty;
        public string RM { set; get; } = string.Empty;

        /// <summary>
        /// 會員編號
        /// </summary>
        public string CustNo { set; get; } = string.Empty;

        /// <summary>
        /// 本次扣點點數
        /// </summary>
        public int UserPoint { set; get; } = 0;

        /// <summary>
        /// 剩餘點數
        /// </summary>
        public int BalancePoint { set; get; } = 0;

        /// <summary>
        /// 回傳內容說明(HTML)
        /// </summary>

        public string ReturnContent { set; get; } = string.Empty;

        /// <summary>
        /// 領取門市代碼
        /// </summary>
        public string Store_ID { set; get; } = string.Empty;
        /// <summary>
        /// 兌換序號
        /// </summary>
        public string Exchange_No { set; get; } = string.Empty;


    }

    public class OrderExchange_No
    {
        public Int64 KEYID { get; set; } = 0;
    }

    public class OrderFromLMS
    {
        public string RC { get; set; } = string.Empty;

        public string RM { get; set; } = string.Empty;

        public string CustNo { get; set; } = string.Empty;

        public string CardNo { get; set; } = string.Empty;

        public string Name { get; set; } = string.Empty;

        public string MobilePhone { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public int Point { get; set; } = 0;

    }

    public class UpdCardPointUse
    {
        public string RC { get; set; } = string.Empty;

        public string RM { get; set; } = string.Empty;

        /// <summary>
        /// 會員編號
        /// </summary>
        public string CustNo { get; set; } = string.Empty;

        /// <summary>
        /// 異動序號
        /// </summary>
        public string PointUseNo { get; set; } = string.Empty;

    }


    public class GetCustCoupon
    {
        public string RC { get; set; } = string.Empty;
        public string RM { get; set; } = string.Empty;

        /// <summary>
        /// 會員編號
        /// </summary>
        public string CustNo { get; set; } = string.Empty;
    }


    /// <summary>
    /// 取得換券通知
    /// </summary>
    public class PushAppCoupon
    {
        public int SysCode { get; set; } = 0;

        public string SysMsg { get; set; } = string.Empty;

        public List<PushAppCouponData> Data { get; set; } = new List<PushAppCouponData>();
    }

    public class PushAppCouponData
    {
        public string UserId { get; set; } = string.Empty;

        public string CouponId { get; set; } = string.Empty;

        public string SerialNo { get; set; } = string.Empty;
        public string Status { get; set; } = string.Empty;
    }

    public class AppCoupon
    {
        public string Amount { get; set; } = string.Empty;

        public List<AppData> Data { get; set; } = new List<AppData>();
    }

    public class AppData
    {
        public string UserId { get; set; } = string.Empty;

        public string CouponId { get; set; } = string.Empty;

        public string Amount { get; set; } = string.Empty;
    }
}


/// <summary>
/// 點數兌換活動資訊頁
/// </summary>
/// 

namespace GetRedeemDetail
{
    public class DetailInput
    {
        /// <summary>
        /// 區域(HK)
        /// </summary>
        /// 

        public string Area { set; get; } = string.Empty;

        /// <summary>
        ///  語系(b)
        /// </summary>
        /// 

        public string Lang { set; get; } = string.Empty;

        /// <summary>
        /// 兌換品編號
        /// </summary>
        /// 

        public string PointRedeemNo { set; get; } = string.Empty;

        /// <summary>
        /// 會員編號
        /// </summary>

        public string CustNo { set; get; } = string.Empty;

    }

    public class DetailOuput
    {
        public string RC { get; set; } = "";

        public string RM { get; set; } = "";

        /// <summary>
        /// 兌換品編號
        /// </summary>
        /// 

        public string PointRedeemNo { get; set; } = "";

        /// <summary>
        /// 主標題
        /// </summary>
        /// 

        public string MainTitle { get; set; } = "";

        /// <summary>
        ///圖片URL
        /// </summary>
        /// 
        public string? ImageUrl { set; get; }
        /// <summary>
        /// 開始日期,格式yyyyMMddHHmmss
        /// </summary>

        public string RedeemStartDate { set; get; } = "";

        /// <summary>
        /// 開始日期,格式yyyyMMddHHmmss
        /// </summary>

        public string RedeemEndDate { set; get; } = "";

        /// <summary>
        /// 注意事項
        /// </summary>
        public string Attention { set; get; } = "";

        // <summary>
        /// 領取方式說明
        /// </summary>

        public string GetwayInfo { set; get; } = "";

        /// <summary>
        /// 活動說明(html)
        /// </summary>
        /// 

        public string RedeemContent { get; set; } = "";

        /// <summary>
        /// 該活動個人可兌換最大數量
        /// </summary>
        /// 

        public int PersonalAllCount { set; get; } = 0;

        /// <summary>
        /// 該活動個人可兌換最大次數(999999:為不限數量)
        /// </summary>
        /// 

        public int? Personal_AllFrequency { set; get; } = 0;

        /// <summary>
        /// 該活動個人剩餘可兌換數量
        /// </summary>

        public int CanRedeemCount { get; set; } = 0;

        /// <summary>
        /// 該活動個人剩餘可兌換次數 (999999:為不限次數)
        /// </summary>

        public int CanRedeemFrequency { get; set; } = 0;


        /// <summary>
        /// 兌換方式列表
        /// </summary>
        /// 

        public List<TypeItem> GetMethodList { get; set; } = new List<TypeItem>();
        /// <summary>
        /// 兌換地點列表
        /// </summary>
        public List<TypeItem> RSList { set; get; } = new List<TypeItem>();

        /// <summary>
        /// 狀態代碼, ON:開放中兌換中,NM: 已兌畢，E:已截止，NS:尚未開始，OL: 已達兌換數量上限,FC:已達兌換次數上限
        /// </summary>
        /// 

        public string StatusCode { set; get; } = "";
        /// <summary>
        /// 狀態名稱，開放中兌換中、已兌畢、已截止、尚未開始、已達兌換數量上限、已達兌換次數上限
        /// </summary>
        public string StatusName { set; get; } = "";

        /// <summary>
        /// 兌換商品清單
        /// </summary>
        /// 

        public List<RedeemProduct> ProductList { set; get; } = new List<RedeemProduct>();

        /// <summary>
        /// 點數積分及加價金額描述
        /// </summary>
        /// 

        public string Point { set; get; } = "";


        /// <summary>
        /// Tag標籤
        /// </summary>
        /// 


        public List<string> EextraTag { set; get; } = new List<string>();

        /// <summary>
        /// 活動類別
        /// </summary>
        /// 

        public string RedeemTypeName { set; get; } = "";

        /// <summary>
        /// 活動說明
        /// </summary>
        /// 

        public string RedeemDesc { set; get; } = "";
    }


    public class TypeItem
    {
        public string CodeKey { get; set; } = "";

        public string Code_Name { get; set; } = "";
    }

    public class RedeemProduct
    {
        /// <summary>
        /// 商品編號
        /// </summary>
        public string RedeemProdNo { set; get; } = "";

        /// <summary>
        /// 圖片URL
        /// </summary>
        /// 

        public string? ImageUrl { set; get; } = "";

        // <summary>
        /// 商品名稱
        /// </summary>
        /// 

        public string ProdName { set; get; } = "";

        /// <summary>
        /// 商品介紹
        /// </summary>

        public string ProdContent { set; get; } = "";

        // <summary>
        /// 兌換(點數)積分
        /// </summary>
        /// 

        public int NeedPoint { set; get; } = 0;

        /// <summary>
        /// 加價
        /// </summary>
        /// 

        public int Plus { get; set; } = 0;

        public int AdditionalPrice { set; get; } = 0;


        /// <summary>
        /// 單次兌換量下限
        /// </summary>

        public int OneTimeMin { set; get; } = 0;

        /// <summary>
        /// 單次兌換量上限
        /// </summary>
        /// 

        public int OneTimeMax { set; get; } = 0;

        /// <summary>
        /// 該商品個人總兌換量上限
        /// </summary>
        /// 

        public int PersonalCount { set; get; } = 0;

        /// <summary>
        /// 庫存量
        /// </summary>

        public int Stock { set; get; } = 0;

        /// <summary>
        /// 狀態代碼, ON:開放中兌換中,NM: 已兌畢，E:已截止，NS:尚未開始，OL: 已達兌換數量上限,FC:已達兌換次數上限
        /// </summary>
        /// 

        public string StatusCode { get; set; } = "";

        /// <summary>
        /// 狀態名稱，開放中兌換中、已兌畢、已截止、尚未開始、已達兌換上限,FC:已達兌換次數上限
        /// </summary>

        public string StatusName { get; set; } = "";

        /// <summary>
        /// 個人可兌換數量(=剩餘數)
        /// </summary>
        /// 

        public int CanExchangeQTY { get; set; }
    }

    public class GetRedeemCount
    {
        /// <summary>
        /// 兌換次數
        /// </summary>
        /// 

        public int RedeemCount { set; get; } = 0;

        /// <summary>
        /// 兌換品項總數
        /// </summary>
        /// 

        public int RedeemSum { set; get; } = 0;
    }
}

/// <summary>
/// 點數兌換活動列表
/// </summary>
/// 

namespace GetRedeemList
{
    public class ListInput
    {
        /// <summary>
        /// 區域(TW)
        /// </summary>
        public string Area { set; get; } = string.Empty;

        /// <summary>
        /// 語系(b)
        /// </summary>
        /// 

        public string Lang { set; get; } = string.Empty;
        /// <summary>
        /// 類別
        /// </summary>
        /// 
        public string Type { get; set; } = string.Empty;


        /// <summary>
        /// 主標題or內容 搜尋關鍵字
        /// </summary>
        /// 

        public string KeyWord { set; get; } = string.Empty;

        /// <summary>
        /// 所需積分
        /// </summary>
        ///
        public string NeedPoint { set; get; } = string.Empty;

        public string CustNo { set; get; } = string.Empty;

        /// <summary>
        /// 資料來源：APP/迷誠品
        /// </summary>
        /// 

        public string DataType { set; get; }

        /// <summary>
        /// 活動狀態
        /// </summary>
        /// 

        public string Status { get; set; }
    }

    public class ListOutput
    {
        public string RC { set; get; } = "";
        public string RM { set; get; } = "";

        public List<ListDetail> RedeemList { set; get; } = new List<ListDetail>();
    }

    [Keyless]

    public class ListDetail
    {
        /// <summary>
        /// 點數活動代碼
        /// </summary>
        /// 

        public string? PointRedeemNo { get; set; } = string.Empty;

        /// <summary>
        /// 主標題
        /// </summary>

        public string? MainTitle { get; set; } = string.Empty;

        /// <summary>
        /// 圖片URL
        /// </summary>
        /// 

        public string? ImageUrl { get; set; } = string.Empty;

        /// <summary>
        ///  點數積分及加價金額描述
        /// </summary>
        /// 

        public string? Point { get; set; } = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        /// 
        public string? AdditionalPrice { set; get; } = string.Empty;

        /// <summary>
        /// 兌換活動分類
        /// </summary>
        /// 

        public string? RedeemType { get; set; } = string.Empty;

        /// <summary>
        /// 兌換活動分類名稱
        /// </summary>
        /// 

        public string? RedeemTypeName { get; set; } = string.Empty;

        public string? RedeemStartDate { get; set; } = string.Empty;

        public string? RedeemEndDate { get; set; } = string.Empty;

        public string? StatusCode { get; set; } = string.Empty;

        public string? StatusName { get; set; } = string.Empty;

        public List<string> ExtraTag
        {
            get
            {
                return new List<string>
                {
                    "限量",
                    "熱門",
                    "季節限定"
                };
            }

            set
            {
                new List<string>
                {
                    "限量",
                    "熱門",
                    "季節限定"
                };
            }
        }

        public string DataType { get; set; } = "HKApp";
    }


}

/// <summary>
/// 取得領取狀態
/// </summary>
/// 

namespace GetReceiveStatus
{
    public class StatusInput
    {
        /// <summary>
        /// 區域(HK)
        /// </summary>
        public string Area { set; get; } = string.Empty;

        /// <summary>
        /// 會員編號
        /// </summary>
        /// 

        public string CustNo { set; get; } = string.Empty;


    }

    public class StatusOutput
    {
        public string RC { set; get; } = "";
        public string RM { set; get; } = "";

        public bool ReceiveStatus { set; get; } = false;

        /// <summary>
        /// 未領取的資料筆數
        /// </summary>
        public int ReceiveCount { set; get; } = 0;
    }
}


/// <summary>
/// 取得類別列表
/// </summary>
/// 

namespace GetTypeLists
{
    public class TypeListsInput
    {
        /// <summary>
        /// 區域(HK)
        /// </summary>
        /// 

        public string strCodeArea { set; get; } = string.Empty;

        /// <summary>
        /// 語系(b)
        /// </summary>
        public string strCodeLang { set; get; } = string.Empty;

        /// <summary>
        /// 類別代碼("AM”:父類別;"AS"子類別)
        /// </summary>
        public string strCodeTypeClass { set; get; } = string.Empty;
        /// <summary>
        /// 子類別代碼(當為空字串則會回傳全部值)
        /// </summary>
        public string strCodeParentClass { set; get; } = string.Empty;
    }

    public class TypeListsOutput
    {
        public string RC { set; get; } = "";
        public string RM { set; get; } = "";

        public List<TypeListsDetail> TypeLists { set; get; } = new List<TypeListsDetail>();
    }

    public class TypeListsDetail
    {
        /// <summary>
        /// 類別群組代碼
        /// </summary>
        public string CodeKey { set; get; } = string.Empty;
        /// <summary>
        /// 類別群組名稱
        /// </summary>
        public string CodeName { set; get; } = string.Empty;
        /// <summary>
        /// 父類別群組代碼
        /// </summary>
        public string ParentKey { set; get; } = string.Empty;
    }
}

/// <summary>
/// 查詢點數專區兌換紀錄
/// </summary>
/// 

namespace GetRedeemLog
{
    public class GetRedeemLogInput
    {
        /// <summary>
        /// 區域(TW)
        /// </summary>
        public string Area { set; get; } = "";
        public string Lang { set; get; } = "";
        public string CustNo { set; get; } = "";
        /// <summary>
        /// 目前無使用
        /// </summary>
        public string PageSize { get; set; } = "";
        /// <summary>
        /// 目前無使用
        /// </summary>
        public string PageNum { get; set; } = "";
    }

    public class GetRedeemLogOutput
    {
        public string RC { set; get; } = "";
        public string RM { set; get; } = "";
        public string CustNo { set; get; } = "";

        public List<RedeemExchangeItem> RedeemExchangeItem = new List<RedeemExchangeItem>();
    }

    public class RedeemExchangeItem
    {
        /// <summary>
        /// 兌換序號
        /// </summary>
        public string ExchangeNo { set; get; } = "";
        /// <summary>
        /// 兌換日期,格式yyyyMMdd
        /// </summary>
        public string ExchangeDate { set; get; } = "";
        /// <summary>
        /// 活動名稱
        /// </summary>
        public string Title { set; get; } = "";
        /// <summary>
        /// 總點數
        /// </summary>
        public int SumPoint { set; get; } = 0;
        public int SumPrice { set; get; } = 0;
        /// <summary>
        /// 狀態
        /// </summary>
        public string Status { set; get; } = "";

        public string StatusCode { set; get; } = "";
        /// <summary>
        /// 領取方式
        /// </summary>
        public string GetWay { set; get; } = "";
        /// <summary>
        /// 店別
        /// </summary>
        public string StoreName { set; get; } = "";
        /// <summary>
        /// 郵寄單號
        /// </summary>
        public string PostNo { set; get; } = "";
        /// <summary>
        /// 領取期限(yyyy/MM/dd)
        /// </summary>
        public string OverReceiveDate { set; get; } = "";

        public string Store_ID { set; get; } = "";
        public string Receiver_Address { set; get; } = "";
        public List<ExchangeItem> ItemList { set; get; } = new List<ExchangeItem>();
    }

    public class ExchangeItem
    {
        public string ItemName { set; get; } = "";
        public int Qty { set; get; } = 0;
        public int Point { set; get; } = 0;
        public int Plus { set; get; } = 0;
        public int AdditionalPrice { set; get; } = 0;
    }
}


/// <summary>
/// 查詢點數兌換紀錄明細
/// </summary>
/// 

namespace GetRedeemLogDetail
{
    public class GetRedeemLogDetailInput
    {
        /// <summary>
        /// 點數兌換紀錄ID
        /// </summary>
        public string ExchangeNo { set; get; } = "";
    }

    public class GetRedeemLogDetailOutput
    {
        public string RC { set; get; } = "";
        public string RM { set; get; } = "";
        /// <summary>
        /// 兌換日期
        /// </summary>
        public string ExchangeDate { set; get; } = "";
        /// <summary>
        /// 活動名稱
        /// </summary>
        public string Title { set; get; } = "";
        /// <summary>
        /// 狀態
        /// </summary>
        public string Status { set; get; } = "";
        /// <summary>
        /// 兌換點數
        /// </summary>
        public int SumPoint { set; get; } = 0;
        /// <summary>
        /// 兌換金額
        /// </summary>
        public int SumPrice { set; get; } = 0;
        /// <summary>
        /// 活動備註
        /// </summary>
        public string RedeemRemark { set; get; } = "";
        /// <summary>
        /// 領取方式
        /// </summary>
        public string GetWay { set; get; } = "";
        /// <summary>
        /// 領取門市id
        /// </summary>
        public string ReceiveStore { set; get; } = "";
        /// <summary>
        /// 領取門市
        /// </summary>
        public string ReceiveStoreName { set; get; } = "";
        /// <summary>
        /// 收貨人
        /// </summary>
        public string Recipient { set; get; } = "";
        /// <summary>
        /// 聯絡電話
        /// </summary>
        public string ReceivePhone { set; get; } = "";
        /// <summary>
        /// 寄送地址
        /// </summary>
        public string Address { set; get; } = "";
        /// <summary>
        /// 訂單備註
        /// </summary>
        public string OrderRemark { set; get; } = "";
        /// <summary>
        /// 領取時間
        /// </summary>
        public string CollectDate { set; get; } = "";

        public List<GetRedeemLogDetailProduct> ProductList { set; get; } = new List<GetRedeemLogDetailProduct>();

    }
    /// <summary>
    /// 兌換商品列表
    /// </summary>
    public class GetRedeemLogDetailProduct
    {
        /// <summary>
        /// 商品id
        /// </summary>
        public int RedeemProdNo { set; get; } = 0;
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string ProdName { set; get; } = "";
        /// <summary>
        /// 商品圖片url
        /// </summary>
        public string ImageUrl { set; get; } = "";
        /// <summary>
        /// 兌換數量
        /// </summary>
        public int RedeemCount { set; get; } = 0;
        /// <summary>
        /// 兌換點數
        /// </summary>
        public int Point { set; get; } = 0;
        /// <summary>
        /// 金額
        /// </summary>
        public int Price { set; get; } = 0;
    }
}

namespace GetCouponSuccessList
{
    public class GetCouponSuccessListInput
    {
        /// <summary>
        /// 地區縮寫(TW:台灣;HK:香港;CN:大陸)
        /// </summary>
        public string Area { get; set; } = string.Empty;
        /// <summary>
        /// 會員編號
        /// </summary>
        public string CustNo { get; set; } = string.Empty;
        /// <summary>
        /// 優惠劵類型
        /// </summary>
        public CouponType CouponType { get; set; }
        /// <summary>
        /// 劵明細陣列
        /// </summary>
        public List<CouponDetail> CouponList { get; set; } = new List<CouponDetail>();
    }

    public class GetCouponSuccessListOutput
    {
        public string RC { set; get; } = "";
        public string RM { set; get; } = "";
    }

    /// <summary>
    /// 劵明細
    /// </summary>
    public class CouponDetail
    {
        /// <summary>
        /// 兌換券ID
        /// </summary>
        public string CouponId { get; set; } = string.Empty;
        /// <summary>
        /// 兌換券序號
        /// </summary>
        public string SerialNo { get; set; } = string.Empty;
        /// <summary>
        /// 劵狀態
        /// </summary>
        public string Status { get; set; } = string.Empty;
        /// <summary>
        /// 兌換日期
        /// </summary>
        public DateTime UsedDate { get; set; } = DateTime.Now;

    }

    /// <summary>
    /// 優惠劵類型
    /// </summary>
    public enum CouponType
    {
        /// <summary>
        /// APP劵
        /// </summary>
        APP = 1,
        /// <summary>
        /// 行旅劵
        /// </summary>
        Travel = 2,
        /// <summary>
        /// LMS劵
        /// </summary>
        LMS = 3
    }
}

namespace CallService
{
    public class CallServiceInput
    {
        /// <summary>
        /// GUID值，自編，用來勾稽回傳狀態用  編號,8碼日期+4碼流水號
        /// </summary>
        public string strGUID { set; get; } = string.Empty;
        /// <summary>
        /// 區域(TW)
        /// </summary>
        public string strArea { set; get; } = string.Empty;
        /// <summary>
        /// 設定環境區域(TW)
        /// </summary>
        public string strServiceArea { set; get; } = string.Empty;
        /// <summary>
        /// 語系(b)
        /// </summary>
        public string strLang { set; get; } = string.Empty;
        /// <summary>
        /// 姓名
        /// </summary>
        public string strName { set; get; } = string.Empty;
        /// <summary>
        /// 會員身分(CardKind/No) 未登入請填入 NO
        /// </summary>
        public string strMemberType { set; get; } = string.Empty;
        /// <summary>
        /// 誠品卡號
        /// </summary>
        public string strCardNo { set; get; } = string.Empty;
        /// <summary>
        /// 行動電話
        /// </summary>
        public string strPhone { set; get; } = string.Empty;
        /// <summary>
        /// 市話
        /// </summary>
        public string strTel { set; get; } = string.Empty;
        /// <summary>
        /// 市話分機
        /// </summary>
        public string strTelExt { set; get; } = string.Empty;
        /// <summary>
        /// 市話區碼
        /// </summary>
        public string strTelAear { set; get; } = string.Empty;
        /// <summary>
        /// 服務父類型
        /// </summary>
        public string strMType { set; get; } = string.Empty;
        /// <summary>
        /// 服務子類型
        /// </summary>
        public string strSType { set; get; } = string.Empty;
        /// <summary>
        /// 電子郵件
        /// </summary>
        public string strEmail { set; get; } = string.Empty;
        /// <summary>
        /// 問題說明
        /// </summary>
        public string strContent { set; get; } = string.Empty;
        /// <summary>
        /// 附檔,將檔案編成base64字串,限制png,jpg,pdf
        /// </summary>
        public string Strfilefilestream1 { set; get; } = string.Empty;
        /// <summary>
        /// Strfilefilestream1的檔名如 png,jpg,pdf
        /// </summary>
        public string Fileextname1 { set; get; } = string.Empty;
        /// <summary>
        /// 附檔,將檔案編成base64字串,限制png,jpg,pdf
        /// </summary>
        public string Strfilefilestream2 { set; get; } = string.Empty;
        /// <summary>
        /// Strfilefilestream2的檔名如 png,jpg,pdf
        /// </summary>
        public string Fileextname2 { set; get; } = string.Empty;
        /// <summary>
        /// 附檔,將檔案編成base64字串,限制png,jpg,pdf
        /// </summary>
        public string Strfilefilestream3 { set; get; } = string.Empty;
        /// <summary>
        /// Strfilefilestream3的檔名,如 png,jpg,pdf
        /// </summary>
        public string Fileextname3 { set; get; } = string.Empty;
        /// <summary>
        /// 是否登入(true/false)
        /// </summary>
        public bool IsLogin { set; get; } = false;
        /// <summary>
        /// 是否為APP(true/false)
        /// </summary>
        public bool IsAPP { set; get; } = true;
        /// <summary>
        /// 回覆方式0 : Email 1 : Phone
        /// </summary>
        public string Reply { set; get; } = string.Empty;
    }

    public class CallServiceOutput
    {
        public string RC { set; get; } = string.Empty;
        public string RM { set; get; } = string.Empty;
        /// <summary>
        /// 發問編號
        /// </summary>
        public string ContactUs_No { set; get; } = string.Empty;
    }

    public class CallServiceSerialNo
    {
        public Int64 KEYID { get; set; } = 0;
    }

}

namespace GetStoreList
{
    public class GetStoreListInput
    {
        /// <summary>
        /// 緯度
        /// </summary>
        public string Latitude { get; set; } = string.Empty;

        /// <summary>
        /// 經度
        /// </summary>
        public string Longitude { get; set; } = string.Empty;

        /// <summary>
        /// 緯度
        /// </summary>
        public double? Lat { get; set; }

        /// <summary>
        /// 經度
        /// </summary>
        public double? Long { get; set; }

        /// <summary>
        /// 縮放比例
        /// </summary>
        public string Zoom { get; set; } = string.Empty;

        /// <summary>
        /// token
        /// </summary>
        public string token { get; set; } = string.Empty;
    }
}
