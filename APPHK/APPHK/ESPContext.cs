﻿

using Microsoft.EntityFrameworkCore.Infrastructure;
using Org.BouncyCastle.Bcpg;
using System;
using System.Collections.Generic;
using System.Reflection.Emit;
public class ESPContext : DbContext
{

    public DbSet<EL_PointRedeem> EL_PointRedeems { get; set; }
    public DbSet<EL_PointRedeem_Product> EL_PointRedeem_Products { get; set; }
    public DbSet<EL_PointRedeem_Product_Stock> EL_PointRedeem_Product_Stocks { get; set; }
    public DbSet<EL_PointRedeem_Success> EL_PointRedeem_Successs { get; set; }
    public DbSet<EL_PointRedeem_Success_Product> EL_PointRedeem_Success_Products { get; set; }
    public DbSet<EL_PointRedeem_Success_Product_Coupon> EL_PointRedeem_Success_Product_Coupons { get; set; }
    public DbSet<tdcode> tdcodes { get; set; }

    public DbSet<EL_Files> EL_Files { get; set; }
    public DbSet<EL_ContactUs> EL_ContactUs { get; set; }
    public DbSet<EL_PointStore> EL_PointStores { get; set; }

    public DbSet<GetRedeemList.ListDetail> ListDetail { get; set; }

    public DbSet<OrderRedeem.OrderExchange_No> OrderExchange_No { get; set; }

    public DbSet<GetRedeemDetail.GetRedeemCount> GetRedeemCount { get; set; }

    public ESPContext()
    {

    }


    public ESPContext(DbContextOptions<ESPContext> options) : base(options)
    {

    }


    //測試機
    protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlServer("Data Source=192.168.75.18;Initial Catalog=ESP_Global; User ID=sa;Password=esliteM052_!*;Trusted_Connection=False; Integrated Security=False;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;");


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<PointRedeemList>().HasNoKey();

        modelBuilder.Entity<GetRedeemDetail.TypeItem>().HasNoKey();

        modelBuilder.Entity<GetRedeemDetail.GetRedeemCount>().HasNoKey();
        modelBuilder.Entity<OrderRedeem.OrderExchange_No>().HasNoKey();

        modelBuilder.Entity<EL_PointStore>(entity => {

            entity.HasKey(e => new { e.Store_ID });

            entity.ToTable("EL_PointStore");

        });

        modelBuilder.Entity<EL_PointRedeem_Product_Stock>(entity => {

            entity.HasKey(e => new { e.prp_SerialNo });

            entity.ToTable("EL_PointRedeem_Product_Stock");

        });

        modelBuilder.Entity<EL_PointRedeem_Product>(entity => {

            entity.HasKey(e => new { e.prp_SerialNo });

            entity.ToTable("EL_PointRedeem_Product");

        });

        modelBuilder.Entity<EL_PointRedeem>(entity => {

            entity.HasKey(e => new { e.PointRedeem_No });

            entity.ToTable("EL_PointRedeem");

        });

        modelBuilder.Entity<EL_Files>(entity => {

            entity.HasKey(e => new { e.f_SerialNo });

            entity.ToTable("EL_Files");

        });

        modelBuilder.Entity<EL_PointRedeem_Success>(entity => {

            entity.HasKey(e => new { e.SeqNo });

            entity.ToTable("EL_PointRedeem_Success");

        });

        modelBuilder.Entity<tdcode>(entity => {

            entity.HasKey(e => new { e.Table_ID, e.Code_Key });

            entity.ToTable("tdcode");

        });

        modelBuilder.Entity<EL_PointRedeem_Success_Product>(entity => {

            entity.HasKey(e => new { e.Exchange_No, e.Item_No });

            entity.ToTable("EL_PointRedeem_Success_Product");

        });

        modelBuilder.Entity<EL_PointRedeem_Success_Product_Coupon>(entity => {

            entity.HasKey(e => new { e.Serial_No });

            entity.ToTable("EL_PointRedeem_Success_Product_Coupon");

        });

        modelBuilder.Entity<EL_ContactUs>(entity => {

            entity.HasKey(e => new { e.ContactUs_No });

            entity.ToTable("EL_ContactUs");

        });

    }



}


public class EL_PointStore
{
    public string Store_ID { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
}
public class EL_PointRedeem
{
    public string PointRedeem_No { get; set; } = string.Empty;

    public string Main_Title { get; set; } = string.Empty;

    public DateTime Redeem_Start_Date { get; set; } = DateTime.Now;
    public DateTime Redeem_End_Date { get; set; } = DateTime.Now;

    public string Point_List { get; set; } = string.Empty;

    public Int64 Image_Url { get; set; } = 0;

    public string Product_Class { get; set; } = string.Empty;
    public string Get_Method { get; set; } = string.Empty;
    public string Get_Desc { get; set; } = string.Empty;
    public string Content { get; set; } = string.Empty;
    public string Attention_Matter { get; set; } = string.Empty;
    public string Redeem_Method { get; set; } = string.Empty;
    public string Key_Word { get; set; } = string.Empty;
    public string Area { get; set; } = string.Empty;
    public string Language { get; set; } = string.Empty;
    public DateTime? Start_Date { get; set; } = DateTime.Now;
    public DateTime? End_Date { get; set; } = DateTime.Now;
    public DateTime? Open_Date { get; set; } = DateTime.Now;
    public DateTime? Close_Date { get; set; } = DateTime.Now;

    public string Close_Reason { get; set; } = string.Empty;

    public Int64 Approve_List { get; set; } = 0;

    public string ModifyUser { get; set; } = string.Empty;

    public DateTime ModifyDate { get; set; } = DateTime.Now;

    public int Sorting { get; set; } = 0;
    public int Personal_AllCount { get; set; } = 0;
    public int? Personal_AllFrequency { get; set; } = 0;

    public string ThisMonth { get; set; } = string.Empty;
    public string AdditionalPrice_List { get; set; } = string.Empty;
    public string? CostUnit { get; set; } = string.Empty;
    public string? PublishDevice { get; set; } = string.Empty;
    public string? PointRedeem_Code { get; set; } = string.Empty;
    public string? BillingUnit { get; set; } = string.Empty;
    public string? DataType { get; set; } = string.Empty;
    public string? PointRedeemNote { get; set; } = string.Empty;


}

public class EL_PointRedeem_Product
{
    public Int64 prp_SerialNo { get; set; }
    public string PointRedeem_No { get; set; } = string.Empty;
    public int Product_No { get; set; }

    public string Product_Name { get; set; } = string.Empty;

    public Int64 Product_ImageUrl { get; set; }
    public Int64 Coupon_ImageUrl { get; set; }

    public int Need_Point { get; set; }
    public int Total_Count { get; set; }
    public int OneTime_Min { get; set; }
    public int OneTime_Max { get; set; }
    public int Personal_TotalCount { get; set; }
    public string Product_Content { get; set; }

    public int Sorting { get; set; }

    public int AdditionalPrice { get; set; }

    public string? APPCouponID { get; set; } = string.Empty;

    public string? LMS_PENO { get; set; } = string.Empty;
    public string? ReleaseType { get; set; } = string.Empty;

    public string? ModifyUser { get; set; } = string.Empty;

    public DateTime? ModifyDate { get; set; }

    public string? CreateUser { get; set; } = string.Empty;

    public DateTime? CreateDate { get; set; }

    //public string? Url { get; set; } = string.Empty;

    //public string? SMSType { get; set; } = string.Empty;


}

public class EL_PointRedeem_Product_Stock
{
    public Int64 prp_SerialNo { get; set; }
    public string PointRedeem_No { get; set; } = string.Empty;
    public int Product_Num { get; set; }
    public int Product_Stock { get; set; }

}

public class EL_Files
{
    public Int64 f_SerialNo { get; set; }
    public string Files_Name { get; set; } = string.Empty;
    public string Files_Url { get; set; } = string.Empty;
    public string Local_Path { get; set; } = string.Empty;
    public string? Related_Table { get; set; } = string.Empty;
    public string? Related_Column { get; set; } = string.Empty;
    public string Create_User { get; set; } = string.Empty;
    public DateTime Create_Date { get; set; } = DateTime.Now;

    public string? Files_OriName { get; set; } = string.Empty;

}

public class EL_PointRedeem_Success
{

    public int SeqNo { get; set; }

    public string Exchange_No { get; set; } = string.Empty;
    public string PointRedeem_No { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public string Card_No { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public string Mobile_Phone { get; set; } = string.Empty;
    public string Get_Method { get; set; } = string.Empty;
    public string Receiver_Name { get; set; } = string.Empty;
    public string Receiver_Address { get; set; } = string.Empty;
    public string? Store_No { get; set; } = string.Empty;
    public string Is_AllCancel { get; set; } = string.Empty;

    public string AllCancel_Reason { get; set; } = string.Empty;
    public DateTime AllCancel_Date { get; set; } = DateTime.Now;
    public string Post_No { get; set; } = string.Empty;
    public string Process_Status { get; set; } = string.Empty;

    public DateTime Exchange_Date { get; set; } = DateTime.Now;

    public string ModifyUser { get; set; } = string.Empty;

    public DateTime ModifyDate { get; set; } = DateTime.Now;

    public string Account_SeqNo { get; set; } = string.Empty;
    public string LMS_C_NO { get; set; } = string.Empty;
    public string ID_Number { get; set; } = string.Empty;

    public DateTime? CollectDate { get; set; } = DateTime.Now;

    public string ExchangeSource { get; set; } = string.Empty;

}

public class EL_PointRedeem_Success_Product
{

    public string Exchange_No { get; set; } = string.Empty;
    public int Item_No { get; set; } = 0;
    public int Product_No { get; set; } = 0;
    public string Product_Name { get; set; } = string.Empty;
    public int Redeem_Count { get; set; } = 0;
    public int Redeem_Point { get; set; } = 0;

    public string Is_Cancel { get; set; } = string.Empty;
    public DateTime? Cancel_Date { get; set; } = DateTime.Now;

    public string DeliveryNo { get; set; } = string.Empty;

    public DateTime? DeliveryDate { get; set; } = DateTime.Now;

    public string DeliveryUser { get; set; } = string.Empty;
    public string DeliveryDep { get; set; } = string.Empty;
    public string IsDelivery { get; set; } = string.Empty;
    public string IsCheck { get; set; } = string.Empty;
    public string CheckUser { get; set; } = string.Empty;
    public string CheckStore { get; set; } = string.Empty;

    public DateTime? CheckTime { get; set; } = DateTime.Now;

    public string DeliveryStatus { get; set; } = string.Empty;
    public int? CouponType { get; set; } = 0;



}

public class EL_PointRedeem_Success_Product_Coupon
{
    public int Serial_No { get; set; } = 0;
    public string Exchange_No { get; set; } = string.Empty;
    public int Product_No { get; set; } = 0;
    public string? Coupon_Id { get; set; } = string.Empty;
    public string? Coupon_Code { get; set; } = string.Empty;
    public string Coupon_status { get; set; } = string.Empty;

    public int Item_No { get; set; } = 0;

    public DateTime? UsedDate { get; set; } = DateTime.Now;
    public DateTime? CreateTime { get; set; } = DateTime.Now;


}

/// <summary>
/// 取得點數兌換列表
/// </summary>
/// 

public class PointRedeemList
{
    /// <summary>
    /// 點數活動代碼
    /// </summary>
    public string PointRedeem_No { get; set; } = string.Empty;
    /// <summary>
    /// 
    /// </summary>
    public string Main_Title { get; set; } = string.Empty;

    public string ImageUrl { get; set; } = string.Empty;

    public string Point { get; set; } = string.Empty;

    public string Plus { get; set; } = string.Empty;

    public string RedeemType { get; set; } = string.Empty;

    public string RedeemTypeName { get; set; } = string.Empty;

    public DateTime RedeemStartDate { get; set; } = DateTime.Now;

    public DateTime RedeemEndDate { get; set; } = DateTime.Now;
    public string StatusCode { get; set; } = string.Empty;
}


/// <summary>
/// 代碼表
/// </summary>
/// 

public class tdcode
{
    public string Table_ID { get; set; } = string.Empty;
    public string Code_Key { get; set; } = string.Empty;
    public string Code_Name { get; set; } = string.Empty;
    public string Code_Char1 { get; set; } = string.Empty;
    public string Code_Char2 { get; set; } = string.Empty;

    public int Sorting { get; set; } = 0;

    public string Is_Locked { get; set; } = string.Empty;
    public string Is_Hidden { get; set; } = string.Empty;
    public string Is_Stop { get; set; } = string.Empty;
}


public class EL_ContactUs
{
    public string ContactUs_No { get; set; } = string.Empty;
    public string Service_Area { get; set; } = string.Empty;
    public string Service_Area_Process { get; set; } = string.Empty;
    public string Account_SeqNo { get; set; } = string.Empty;
    public string Member_Type { get; set; } = string.Empty;
    public string Card_No { get; set; } = string.Empty;
    public string Ask_MainType { get; set; } = string.Empty;
    public string Ask_SubType { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public string E_Mail { get; set; } = string.Empty;
    public string MobilePhone { get; set; } = string.Empty;
    public string Tel { get; set; } = string.Empty;
    public string Ext { get; set; } = string.Empty;
    public string Ask_Content { get; set; } = string.Empty;
    public Int64 Attach_FileUrl { get; set; } = 0;
    public Int64 Attach_FileUrl2 { get; set; } = 0;
    public Int64 Attach_FileUrl3 { get; set; } = 0;
    public string Area { get; set; } = string.Empty;
    public string Language { get; set; } = string.Empty;
    public DateTime Ask_Date { get; set; } = DateTime.Now;
    public string ContactUs_Source { get; set; } = string.Empty;
    public string Ask_MainType_B { get; set; } = string.Empty;
    public string Ask_SubType_B { get; set; } = string.Empty;
    public string SourceIP { get; set; } = string.Empty;
    public string InternetAccount { get; set; } = string.Empty;
    public string OrderID { get; set; } = string.Empty;
    public string OrderUrl { get; set; } = string.Empty;
    public string CustNo { get; set; } = string.Empty;
    public string CustUrl { get; set; } = string.Empty;
    public string DeviceBrowser { get; set; } = string.Empty;
    public bool IsEC { get; set; } = false;

}