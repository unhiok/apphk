using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using OrderRedeem;
using System;
using static System.Net.WebRequestMethods;

var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<ESPContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("ESPConnection"));
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

/// <summary>
/// 翴计传笆
/// </summary>

app.MapPost("GetRedeemList_hk", (GetRedeemList.ListInput input) =>
{

    using (var db = new ESPContext())
    {

        var GetPointRedeemList = new GetRedeemList.ListOutput();

        var ListDetail = db.Set<GetRedeemList.ListDetail>()
             .FromSqlRaw("exec sp_GetPointRedeem_List_HK {0},{1},{2},{3},{4},null,null,null,{5},null,null,{6}", input.Area, input.Lang, input.Type, input.KeyWord, input.NeedPoint, input.CustNo, input.DataType)
             .ToList();
        //TWApp,WEB,
        GetPointRedeemList.RC = "0";
        GetPointRedeemList.RM = "Θ";
        GetPointRedeemList.RedeemList = ListDetail;

        return GetPointRedeemList;
    }


}).WithName("GetRedeemList_hk");


/// <summary>
/// 翴计传笆戈癟
/// </summary>
/// 


app.MapPost("RedeemPoints/GetRedeemDetail_hk", (GetRedeemDetail.DetailInput input) =>
{
    string img_pre = @"https://www.esliteliving.com/";

    using (var db = new ESPContext())
    {
        var GetRedeemDetail = new GetRedeemDetail.DetailOuput();

        var Redeem = db.EL_PointRedeems.Where(x => x.PointRedeem_No.Equals(input.PointRedeemNo)).FirstOrDefault();

        var files = db.EL_Files.Where(x => x.Related_Table.Equals("EL_PointRedeem"));


        var GetRedeemCount_List = db.Set<GetRedeemDetail.GetRedeemCount>()
            .FromSqlRaw("select * from GetRedeemCount('{0}','{1}')", input.CustNo, input.PointRedeemNo)
            .ToList();

        var GetRedeemCount = GetRedeemCount_List[0];

        if (Redeem != null)
        {
            GetRedeemDetail.PointRedeemNo = Redeem.PointRedeem_No;
            GetRedeemDetail.MainTitle = Redeem.Main_Title;
            GetRedeemDetail.RedeemStartDate = Redeem.Redeem_Start_Date.ToString("yyyyMMddHHmmss");
            GetRedeemDetail.Attention = Redeem.Attention_Matter;
            GetRedeemDetail.GetwayInfo = Redeem.Get_Desc;
            GetRedeemDetail.RedeemContent = Redeem.Content;
            GetRedeemDetail.PersonalAllCount = Redeem.Personal_AllCount;
            GetRedeemDetail.Personal_AllFrequency = Redeem.Personal_AllFrequency;
            GetRedeemDetail.Point = Redeem.Point_List;
            GetRedeemDetail.RedeemDesc = Redeem.Content;
            GetRedeemDetail.EextraTag.Add("﹗竊﹚");
            GetRedeemDetail.EextraTag.Add("穦盡妮");

            GetRedeemDetail.CanRedeemCount = Redeem.Personal_AllCount - GetRedeemCount.RedeemSum;
            GetRedeemDetail.CanRedeemFrequency = Redeem.Personal_AllFrequency ?? 0 - GetRedeemCount.RedeemCount;

            var redeemImage = files.Where(x => x.f_SerialNo.Equals(Redeem.Image_Url)).FirstOrDefault();

            if (redeemImage != null)
            {
                GetRedeemDetail.ImageUrl = img_pre + redeemImage.Files_Url;
            }
            else
            {
                GetRedeemDetail.ImageUrl = "";
            }

            #region 篈

            if (DateTime.Now.CompareTo(Redeem.Redeem_Start_Date) >= 0
            && DateTime.Now.CompareTo(Redeem.Redeem_End_Date) <= 0)
            {
                GetRedeemDetail.StatusCode = "ON";
                GetRedeemDetail.StatusName = "秨传い";


                if (GetRedeemDetail.CanRedeemCount <= 0)
                {
                    GetRedeemDetail.StatusCode = "OL";
                    GetRedeemDetail.StatusName = "笷传计秖";
                }
                else if (GetRedeemDetail.CanRedeemFrequency <= 0)
                {
                    GetRedeemDetail.StatusCode = "FC";
                    GetRedeemDetail.StatusName = "笷传Ω计";
                }

                var stockSum = db.EL_PointRedeem_Product_Stocks.Where(x => x.PointRedeem_No.Equals(input.PointRedeemNo)).Sum(x => x.Product_Stock);

                if (stockSum <= 0)
                {
                    GetRedeemDetail.StatusCode = "NM";
                    GetRedeemDetail.StatusName = "拨";
                }

                if (DateTime.Now.CompareTo(Redeem.Redeem_Start_Date) < 0)
                {
                    GetRedeemDetail.StatusCode = "NS";
                    GetRedeemDetail.StatusName = "﹟ゼ秨﹍";
                }

                if (DateTime.Now.CompareTo(Redeem.Redeem_End_Date) > 0)
                {
                    GetRedeemDetail.StatusCode = "E";
                    GetRedeemDetail.StatusName = "篒ゎ";
                }
            }

            #endregion

            #region GetMethodList  传よΑ  
            var GetMethodList = db.Set<GetRedeemDetail.TypeItem>()
            .FromSqlRaw("select Code_Key as CodeKey,Code_Name from tdcode where Is_Hidden = 'N' and Is_Stop = 'N' and  Table_ID = 'EL_RedeemGetMethod'");

            if (Redeem.Get_Method != "A" || Redeem.Get_Method != "N")
            {
                GetRedeemDetail.GetMethodList = GetMethodList.Where(x => x.CodeKey.Equals(Redeem.Get_Method)).ToList();
            }
            #endregion


            #region RSList 传翴
            var RSList = db.Set<GetRedeemDetail.TypeItem>()
        .FromSqlRaw("select Store_No as CodeKey,Name as Code_Name from v_PointRedeem_StoreList where Column_No = {0} order by Sorting", input.PointRedeemNo)
        .ToList();

            GetRedeemDetail.RSList = RSList;

            #endregion

            #region RedeemTypeName	笆摸
            #endregion

            #region 传坝珇睲虫

            var ProudClassList = db.Set<GetRedeemDetail.TypeItem>()
                                .FromSqlRaw("select Code_Key as CodeKey,Code_Name from tdcode where Is_Hidden = 'N' and Is_Stop = 'N' and Table_ID ='EL_RedeemProductClass'")
                                .ToList();

            var RedeemType = ProudClassList.Where(x => x.CodeKey.Equals(Redeem.Product_Class)).FirstOrDefault();
            if (RedeemType != null)
            {
                GetRedeemDetail.RedeemTypeName = RedeemType.Code_Name;
            }

            #endregion

            # region ProductList 传坝珇睲虫

            List<GetRedeemDetail.RedeemProduct> ProductList = new List<GetRedeemDetail.RedeemProduct>();

            var product = db.EL_PointRedeem_Products.Where(x => x.PointRedeem_No.Equals(input.PointRedeemNo)).ToList();

            foreach (var item in product)
            {
                var productImg = files.Where(x => x.f_SerialNo.Equals(item.Product_ImageUrl)).FirstOrDefault();

                var stock = db.EL_PointRedeem_Product_Stocks.FirstOrDefault(x => x.prp_SerialNo.Equals(item.prp_SerialNo));

                string sql = string.Format("select * from GetRedeemProductCount('{0}','{1}','{2}')", input.CustNo, input.PointRedeemNo, item.prp_SerialNo);

                var GetRedeemProductCount_List = db.Set<GetRedeemDetail.GetRedeemCount>()
                .FromSqlRaw(sql)
                .ToList();

                var GetRedeemProductCount = GetRedeemProductCount_List[0];

                var CanExchangeQTY = item.Personal_TotalCount - GetRedeemCount.RedeemSum;

                string StatusCode = GetRedeemDetail.StatusCode;
                string StatusName = GetRedeemDetail.StatusName;

                int Stock = stock?.Product_Stock ?? 0;

                if (Stock == 0)
                {
                    StatusCode = "NM";
                    StatusName = "拨";
                }
                if (CanExchangeQTY <= 0)
                {
                    StatusCode = "OL";
                    StatusName = "笷传";
                }

                ProductList.Add(new GetRedeemDetail.RedeemProduct
                {
                    RedeemProdNo = item.prp_SerialNo.ToString(),
                    ProdName = item.Product_Name,
                    ProdContent = item.Product_Content,
                    ImageUrl = productImg != null ? img_pre + productImg.Files_Url : "",
                    NeedPoint = item.Need_Point,
                    AdditionalPrice = item.AdditionalPrice,
                    Plus = item.AdditionalPrice,
                    OneTimeMax = item.OneTime_Max,
                    OneTimeMin = item.OneTime_Min,
                    PersonalCount = item.Personal_TotalCount,
                    StatusCode = StatusCode,
                    StatusName = StatusName,

                    Stock = Stock,
                    CanExchangeQTY = CanExchangeQTY

                });

            }


            GetRedeemDetail.ProductList = ProductList;
            #endregion

        }

        GetRedeemDetail.RC = "0";
        GetRedeemDetail.RM = "Θ";
        return GetRedeemDetail;
    }
}).WithName("GetRedeemDetail_hk");


/// <summary>
/// 秈︽翴计传
/// </summary

//GetRedeemDetail 
//OrderDetail
//狦Τarea耞╊秨ぃ璶ノ

//app.MapPost("RedeemPoints/OrderRedeem_hk", async (OrderRedeem.OrderInput input) =>
//{
//    #region 代刚跋

//    //穦キapi
//    var lmsapi = @"http://10.200.129.225:8080/wsLMSHK/GetCustDataJSON";


//    #endregion





//    //眔穦戈

//    using (var db = new ESPContext())
//    {

//        var GetOrderRedeemList = new OrderRedeem.OrderOutput();
//        //var GetRedeem = db.EL_PointRedeems.FirstOrDefault(x => x.PointRedeem_No.Equals(input.PointRedeemNo));
//        string product_list = "";

//        var values = new Dictionary<string, string>
//            {
//                 { "strCustNo", input.CustNo}
//            };

//        HttpClient client = new HttpClient();

//        var content = new FormUrlEncodedContent(values);

//        var response = client.PostAsync(lmsapi, content);

//        var OrderFromLMS = response.Result.Content.ReadFromJsonAsync<OrderRedeem.OrderFromLMS>().Result;




//        //switch (input.ReceiveWay)
//        //{
//        //    case "M":
//        //        #region M:秎盚
//        //        if (string.IsNullOrEmpty(input.Recipient) || string.IsNullOrEmpty(input.Address) || string.IsNullOrEmpty(input.ReceivePhone))
//        //        {
//        //            GetOrderRedeemList.CustNo = input.CustNo;
//        //            GetOrderRedeemList.RC = "-98";
//        //            GetOrderRedeemList.RM = "肚把计Τ粇";
//        //            return GetOrderRedeemList;
//        //        }
//        //        #endregion
//        //        break;

//        //    case "N":
//        //        #region N: 礚

//        //        #endregion
//        //        break;
//        //    case "S":
//        //        #region S:カ砯
//        //        if (string.IsNullOrEmpty(input.ReceiveStore))
//        //        {
//        //            GetOrderRedeemList.CustNo = input.CustNo;
//        //            GetOrderRedeemList.RC = "-98";
//        //            GetOrderRedeemList.RM = "肚把计Τ粇";
//        //            return GetOrderRedeemList;
//        //        }
//        //        #endregion
//        //        break;

//        //    case "TWAPP":
//        //        #region HKAPP:港珇eslite APP

//        //        #endregion
//        //        break;
//        //}

//        //if (GetRedeem != null)
//        //{
//        //    #region 弄穦api

//        //    #endregion

//        //    var Exchange_No_List = db.Set<OrderRedeem.OrderExchange_No>().
//        //    FromSqlRaw("exec Insert_EL_PointRedeem_Success {0},{1},{2},{3},{4},{5},{6},{7},'',{8},{9},'','','',{10}",
//        //    input.PointRedeemNo, OrderFromLMS?.Name ?? "", OrderFromLMS?.Email ?? "", OrderFromLMS?.MobilePhone,
//        //    input.ReceiveWay, input.Recipient, input.Address, input.ReceiveStore, OrderFromLMS.CustNo ?? "", OrderFromLMS?.CardNo ?? "", input.DataType).ToList();

//        //    var Exchange_No = Exchange_No_List[0];

//        //    if(Exchange_No != null)
//        //    {
//        //        foreach(var item in input.OrderList)
//        //        {
//        //            var RedeemProdNo = Convert.ToInt64(item.RedeemProdNo);
//        //            var product = db.EL_PointRedeem_Products.FirstOrDefault(x => x.prp_SerialNo.Equals(RedeemProdNo));
//        //            var stock = db.EL_PointRedeem_Product_Stocks.FirstOrDefault(x => x.prp_SerialNo.Equals(RedeemProdNo));

//        //            if(stock != null)
//        //            {
//        //                #region 2.絋粄翴计坝珇畐ì镑

//        //                if (stock.Product_Stock >= item.QTY)
//        //                {
//        //                    //Ι畐
//        //                    stock.Product_Stock = stock.Product_Stock - item.QTY;
//        //                    db.Entry(stock).State = EntityState.Modified;
//        //                    db.SaveChanges();
//        //                }

//        //                #endregion

//        //                if(product != null)
//        //                {

//        //                }
//        //            }
//        //        }
//        //    }

//        //}
//        return GetOrderRedeemList;
//    }


//}).WithName("OrderRedeem_hk");


app.Run();

